<?php


namespace Spacecat\Feishu;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FeishuCloudDocument
{
    /**
     * ============= 云空间 =============
     */

    /**
     * 新建文件夹
     *
     * @param $tenantAccessToken
     * @param $folderToken
     * @param $name
     * @return mixed
     * @throws GuzzleException
     */
    public static function newFolderV2($tenantAccessToken, $folderToken, $name): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/drive/v1/files/create_folder", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => [
            'name' => $name,
            'folderToken' => $folderToken,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取文件夹下文档清单
     *
     * @param $accessToken
     * @param $folderToken
     * @param null $types
     * @return mixed
     * @throws GuzzleException
     */
    public static function getDocumentsListInFolder($accessToken, $folderToken, $types = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/drive/explorer/v2/folder/$folderToken/children", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => ['types' => $types]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 上传文件
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function uploadFile($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/drive/v1/files/upload_all', ['headers' => [
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'multipart' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 分片上传文件（预上传）
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function uploadFileInFragmentsPreUpload($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/drive/v1/files/upload_prepare', ['headers' => [
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 分片上传文件（上传分片）
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function uploadFileInFragmentsUploadFragment($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/drive/v1/files/upload_part', ['headers' => [
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'multipart' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 分片上传文件（完成上传）
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function uploadFileInFragmentsFinishUpload($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/drive/v1/files/upload_finish', ['headers' => [
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 复制文档
     *
     * @param $accessToken
     * @param $fileToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function copyFile($accessToken, $fileToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/drive/explorer/v2/file/copy/files/$fileToken", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 删除 Doc
     *
     * @param $accessToken
     * @param $docToken
     * @return mixed
     * @throws GuzzleException
     */
    public static function deleteDoc($accessToken, $docToken): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('DELETE', "/open-apis/drive/explorer/v2/file/docs/$docToken", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * ============= 权限 =============
     */

    /**
     * 增加权限
     *
     * @param $accessToken
     * @param $fileToken
     * @param $query
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function addPermission($accessToken, $fileToken, $query, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/drive/v1/permissions/$fileToken/members", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 更新协作者权限
     *
     * @param $accessToken
     * @param $fileToken
     * @param $memberId
     * @param $query
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function updateCollaboratorPermissions($accessToken, $fileToken, $memberId, $query, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('PUT', "/open-apis/drive/v1/permissions/$fileToken/members/$memberId", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 移除协作者权限
     *
     * @param $accessToken
     * @param $fileToken
     * @param $memberId
     * @param $query
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function removeCollaboratorPermissions($accessToken, $fileToken, $memberId, $query, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('DELETE', "/open-apis/drive/v1/permissions/$fileToken/members/$memberId", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 判断当前用户对某文档是否有某权限
     *
     * @param $accessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function checkCollaboratorHasPermission($accessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/drive/permission/member/permitted", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取协作者列表
     *
     * @param $accessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function getCollaboratorList($accessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/drive/permission/member/list", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 更新文档公共设置
     *
     * @param $accessToken
     * @param $fileToken
     * @param $type
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function updateDocumentPublicConfig($accessToken, $fileToken, $type, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('PATCH', "/open-apis/drive/v1/permissions/$fileToken/public", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => ['type' => $type], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * ============= 评论 =============
     */

    /**
     * 获取评论列表
     *
     * @param $accessToken
     * @param $fileToken
     * @param $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getCommentList($accessToken, $fileToken, $query): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/drive/v1/files/$fileToken/comments", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * ============= 文档 =============
     */

    /**
     * 创建文档
     *
     * @param $accessToken
     * @param null $folderToken
     * @param null $content
     * @return mixed
     * @throws GuzzleException
     */
    public static function createDoc($accessToken, $folderToken = null, $content = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/doc/v2/create", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'json' => [
            'FolderToken' => $folderToken,
            'Content' => $content,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 文档-新版文档-创建文档
     * @param $accessToken
     * @param null $folderToken
     * @param null $title
     * @return mixed
     * @throws GuzzleException
     */
    public static function newDocumentCreateDoc($accessToken, $folderToken = null, $title = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/docx/v1/documents", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'json' => [
            'folder_token' => $folderToken,
            'title' => $title,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取文档所有块
     * @param $accessToken
     * @param $documentId
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getDocAllBlocks($accessToken, $documentId, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/docx/v1/documents/$documentId/blocks", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 文档-新版文档-块-创建块
     *
     * @param $accessToken
     * @param $documentId
     * @param $blockId
     * @param array $query
     * @param array $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function newDocumentBlocksCreateBlock($accessToken, $documentId, $blockId, array $query = [], array $postData = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/docx/v1/documents/$documentId/blocks/$blockId/children", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }
    public static function createBlock($accessToken, $documentId, $blockId, array $query = [], array $postData = [])
    {
        return self::newDocumentBlocksCreateBlock($accessToken, $documentId, $blockId, $query, $postData);
    }

    /**
     * 更新块
     * @param $accessToken
     * @param $documentId
     * @param $blockId
     * @param array $query
     * @param array $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function updateBlock($accessToken, $documentId, $blockId, array $postData = [], array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('PATCH', "/open-apis/docx/v1/documents/$documentId/blocks/$blockId", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 批量更新块
     *
     * @param $accessToken
     * @param $documentId
     * @param array $postData
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function batchUpdateBlocks($accessToken, $documentId, array $postData = [], array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('PATCH', "/open-apis/docx/v1/documents/$documentId/blocks/batch_update", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 文档-新版文档-块-删除块
     *
     * @param $accessToken
     * @param $documentId
     * @param $blockId
     * @param array $query
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function newDocumentBlocksDeleteBlock($accessToken, $documentId, $blockId, $postData, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('DELETE', "/open-apis/docx/v1/documents/$documentId/blocks/$blockId/children/batch_delete", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 文档-新版文档-块-删除块（建议使用新的方法名 newDocumentBlocksDeleteBlock）
     *
     * @param $accessToken
     * @param $documentId
     * @param $blockId
     * @param $postData
     * @param array $query
     * @throws GuzzleException
     */
    public static function deleteBlock($accessToken, $documentId, $blockId, $postData, array $query = [])
    {
        self::newDocumentBlocksDeleteBlock($accessToken, $documentId, $blockId, $postData, $query);
    }

    /**
     * ============= 电子表格 =============
     */

    /**
     * 创建表格
     *
     * @param $accessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function newSpreadsheet($accessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/sheets/v3/spreadsheets', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取表格元数据
     *
     * @param $spreadsheetToken
     * @param $tenantAccessToken
     * @param null $extFields
     * @return mixed
     * @throws GuzzleException
     */
    public static function getTableMetadata($spreadsheetToken, $tenantAccessToken, $extFields = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/sheets/v2/spreadsheets/$spreadsheetToken/metainfo", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => [
            'extFields' => $extFields,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 追加数据（电子表格）
     *
     * @param $accessToken
     * @param $spreadsheetToken
     * @param $postData
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function appendDataToSpreadSheet($accessToken, $spreadsheetToken, $postData, $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/sheets/v2/spreadsheets/$spreadsheetToken/values_append", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 读取单个范围
     *
     * @param $spreadsheetToken
     * @param $tenantAccessToken
     * @param $range
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function readSingleRange($spreadsheetToken, $tenantAccessToken, $range, $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/sheets/v2/spreadsheets/$spreadsheetToken/values/$range", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 向单个范围写入数据
     *
     * @param $spreadsheetToken
     * @param $tenantAccessToken
     * @param $valueRange
     * @return mixed
     * @throws GuzzleException
     */
    public static function writeDataToSingleRange($spreadsheetToken, $tenantAccessToken, $valueRange): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('PUT', "/open-apis/sheets/v2/spreadsheets/$spreadsheetToken/values", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => [
            'valueRange' => $valueRange,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * ============= 多维表格 =============
     */

    /**
     * 多维表格-记录-列出记录
     *
     * @param $tenantAccessToken
     * @param $appToken
     * @param $tableId
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function multiTableRecordsListRecords($accessToken, $appToken, $tableId, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/bitable/v1/apps/$appToken/tables/$tableId/records", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 多维表格-记录-新增多条记录
     *
     * @param $accessToken
     * @param $appToken
     * @param $tableId
     * @param $postData
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function multiTableRecordsAddRecords($accessToken, $appToken, $tableId, $postData, $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/bitable/v1/apps/$appToken/tables/$tableId/records/batch_create", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 多维表格-记录-删除多条记录
     *
     * @param $accessToken
     * @param $appToken
     * @param $tableId
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function multiTableRecordsRemoveRecords($accessToken, $appToken, $tableId, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/bitable/v1/apps/$appToken/tables/$tableId/records/batch_delete", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * ============= 历史版本（不推荐） =============
     */

    /**
     * 新建文件夹（历史版本，不推荐）
     *
     * @param $tenantAccessToken
     * @param $folderToken
     * @param $title
     * @return mixed
     * @throws GuzzleException
     */
    public static function newFolder($tenantAccessToken, $folderToken, $title): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/drive/explorer/v2/folder/$folderToken", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => ['title' => $title]]);
        return json_decode($response->getBody()->getContents());
    }
}
