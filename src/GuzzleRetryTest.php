<?php

namespace Spacecat\Feishu;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class GuzzleRetryTest
{
    /**
     * 测试重试中间件
     *
     * @throws GuzzleException
     */
    public static function test(): \Psr\Http\Message\StreamInterface
    {
        $middleware = GuzzleRetry::createRetryMiddleware();
        $request = new Request('GET', '');
        $connectException = new ConnectException(
            'Connection failed',
            $request
        );
        $handler = new MockHandler([
            $connectException,
            $connectException,
            $connectException,
            new Response(200, [], 'ok'),
        ]);
        $client = new Client(['handler' => $middleware($handler)]);
        $response = $client->send($request);
        return $response->getBody();
    }
}
