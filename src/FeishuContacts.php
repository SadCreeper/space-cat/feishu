<?php


namespace Spacecat\Feishu;


use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FeishuContacts
{
    /**
     * 创建用户
     *
     * @param $tenantAccessToken
     * @param $postData
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function createUser($tenantAccessToken, $postData, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/contact/v3/users', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取单个用户信息
     *
     * @param $tenantAccessToken
     * @param $userId
     * @param string $userIdType
     * @param string $department_id_type
     * @return mixed
     * @throws GuzzleException
     */
    public static function getSingleUserData($tenantAccessToken, $userId, string $userIdType = 'open_id', string $department_id_type = 'open_department_id'): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/contact/v3/users/' . $userId, ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => [
            'user_id_type' => $userIdType,
            'department_id_type' => $department_id_type,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 批量获取用户信息
     *
     * @param $tenantAccessToken
     * @param array $userIds
     * @param string $userIdType
     * @param string $department_id_type
     * @return mixed
     * @throws GuzzleException
     */
    public static function getBatchUsersData($tenantAccessToken, array $userIds, string $userIdType = 'open_id', string $department_id_type = 'open_department_id'): mixed
    {
        $params = [
            "user_id_type=$userIdType",
            "department_id_type=$department_id_type",
        ];
        foreach ($userIds as $userId) {
            $params[] = "user_ids=$userId";
        }
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/contact/v3/users/batch?' . implode('&', $params), ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ]]);
        return json_decode($response->getBody()->getContents());
    }


    /**
     * 通过手机号或邮箱获取用户 ID
     *
     * https://open.feishu.cn/document/uAjLw4CM/ukTMukTMukTM/reference/contact-v3/user/batch_get_id
     * @param $tenantAccessToken
     * @param array $postData
     * @param null $userIdType
     * @return mixed
     * @throws GuzzleException
     */
    public static function getUserIdByPhoneNumberOrEmailV3($tenantAccessToken, array $postData, $userIdType = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/contact/v3/users/batch_get_id', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['user_id_type' => $userIdType], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取部门直属用户列表
     *
     * @param $access_token
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getUserListUnderDepartment($access_token, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/contact/v3/users/find_by_department', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $access_token,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取部门直属用户列表『全部不分页』
     *
     * @param $access_token
     * @param array $query
     * @return array
     * @throws GuzzleException
     */
    public static function getUserListUnderDepartmentAll($access_token, array $query = []): array
    {
        $users = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = FeishuContacts::getUserListUnderDepartment($access_token, array_merge([
                'page_token' => $pageToken,
                'page_size' => 50,
            ], $query));
            $hasMore = $res->{'data'}->{'has_more'};
            $pageToken = $hasMore ? $res->{'data'}->{'page_token'} : null;
            $users = array_merge($users, property_exists($res->data, 'items') ? $res->data->items : []);
        }
        return $users;
    }

    /**
     * 修改用户部分信息
     *
     * @param $tenantAccessToken
     * @param $userId
     * @param $postData
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function modifyUserPartialData($tenantAccessToken, $userId, $postData, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('PATCH', "/open-apis/contact/v3/users/$userId", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取单个部门信息
     *
     * @param $tenantAccessToken
     * @param $departmentId
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getSingleDepartmentData($tenantAccessToken, $departmentId, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/contact/v3/departments/' . $departmentId, ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取子部门列表
     *
     * @param $accessToken
     * @param $departmentId
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getChildDepartmentList($accessToken, $departmentId, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/contact/v3/departments/$departmentId/children", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取子部门列表『全部不分页』
     *
     * @param $accessToken
     * @param $departmentId
     * @param array $query
     * @return array
     * @throws GuzzleException
     */
    public static function getChildDepartmentListAll($accessToken, $departmentId, array $query = []): array
    {
        $departments = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = FeishuContacts::getChildDepartmentList($accessToken, $departmentId, array_merge([
                'page_token' => $pageToken,
                'page_size' => 50,
            ], $query));
            $hasMore = $res->{'data'}->{'has_more'};
            $pageToken = $hasMore ? $res->{'data'}->{'page_token'} : null;
            $departments = array_merge($departments, property_exists($res->data, 'items') ? $res->data->items : []);
        }
        return $departments;
    }

    /**
     * 获取父部门信息
     *
     * @param $tenantAccessToken
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getParentDepartmentsData($tenantAccessToken, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/contact/v3/departments/parent', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取父部门信息『全部不分页』
     *
     * @param $tenantAccessToken
     * @param array $query
     * @return array
     * @throws GuzzleException
     */
    public static function getParentDepartmentsDataAll($tenantAccessToken, array $query = []): array
    {
        $departments = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = FeishuContacts::getParentDepartmentsData($tenantAccessToken, array_merge([
                'page_token' => $pageToken,
                'page_size' => 50,
            ], $query));
            $hasMore = $res->{'data'}->{'has_more'};
            $pageToken = $hasMore ? $res->{'data'}->{'page_token'} : null;
            $departments = array_merge($departments, property_exists($res->data, 'items') ? $res->data->items : []);
        }
        return $departments;
    }

    /**
     * ============= 历史版本（不推荐） =============
     */

    /**
     * 使用手机号或邮箱获取用户 ID（历史版本，不推荐）
     *
     * @param $tenantAccessToken
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getUserIdByPhoneNumberOrEmail($tenantAccessToken, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/user/v1/batch_get_id', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取用户列表（历史版本，不推荐）
     *
     * @param $tenantAccessToken
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     * @throws Exception
     */
    public static function getUserList($tenantAccessToken, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/contact/v3/users", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取用户列表『全部不分页』（历史版本，不推荐）
     *
     * @param $tenantAccessToken
     * @param array $query
     * @return array
     * @throws GuzzleException
     */
    public static function getUserListAll($tenantAccessToken, array $query = []): array
    {
        $users = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = FeishuContacts::getUserList($tenantAccessToken, array_merge([
                'page_token' => $pageToken,
                'page_size' => 50,
            ], $query));
            $hasMore = $res->{'data'}->{'has_more'};
            $pageToken = $hasMore ? $res->{'data'}->{'page_token'} : null;
            $users = array_merge($users, property_exists($res->data, 'items') ? $res->data->items : []);
        }
        return $users;
    }

    /**
     * 获取部门信息列表（历史版本，不推荐）
     *
     * @param $tenantAccessToken
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getDepartmentDataList($tenantAccessToken, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/contact/v3/departments', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取部门信息列表『全部不分页』（历史版本，不推荐）
     *
     * @param $tenantAccessToken
     * @param array $query
     * @return array
     * @throws GuzzleException
     */
    public static function getDepartmentDataListAll($tenantAccessToken, array $query = []): array
    {
        $departments = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = FeishuContacts::getDepartmentDataList($tenantAccessToken, array_merge([
                'page_token' => $pageToken,
                'page_size' => 50,
            ], $query));
            $hasMore = $res->{'data'}->{'has_more'};
            $pageToken = $hasMore ? $res->{'data'}->{'page_token'} : null;
            $departments = array_merge($departments, property_exists($res->data, 'items') ? $res->data->items : []);
        }
        return $departments;
    }

}
