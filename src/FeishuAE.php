<?php

namespace Spacecat\Feishu;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FeishuAE
{
    /**
     * 获取 appToken
     * @param $clientId
     * @param $clientSecret
     * @return mixed
     * @throws GuzzleException
     */
    public static function getAppToken($clientId, $clientSecret): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_AE, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/auth/v1/appToken', ['json' => [
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取 tenant_access_token
     * @param $appToken
     * @param $namespace
     * @return mixed
     * @throws GuzzleException
     */
    public static function getTenantAccessToken($appToken, $namespace): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_AE, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/api/integration/v1/namespaces/${namespace}/defaultLark/tenantAccessToken", [
            'headers' => [
                'Authorization' => $appToken
            ]
        ]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 查询 aPaas 用户的飞书 user_id
     * @param $appToken
     * @param $aPaasUserId
     * @return null
     * @throws GuzzleException
     */
    public static function getFsUserId($appToken, $aPaasUserId)
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_AE, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/api/integration/v2/feishu/getUsers", [
            'headers' => [
                'Authorization' => $appToken
            ],
            'json' => [
                'user_id_type' => 'user_id',
                'user_ids' => [$aPaasUserId]
            ]
        ]);
        $response = json_decode($response->getBody()->getContents());
        $data = $response->data;
        return sizeof($data) ? $data[0]->external_user_id : null;
    }

    /**
     * 查询对象记录列表
     * @param $appToken
     * @param $namespace
     * @param $object
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function getObjectRecords($appToken, $namespace, $object, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_AE, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/api/data/v1/namespaces/$namespace/objects/$object/records", [
            'headers' => [
                'Authorization' => $appToken
            ],
            'json' => $postData,
        ]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 查询对象记录列表（全部不分页）
     * @param $appToken
     * @param $namespace
     * @param $object
     * @param $postData
     * @param $callback
     * @return array
     * @throws GuzzleException
     */
    public static function getObjectAllRecords($appToken, $namespace, $object, $postData, $callback = null): array
    {
        $minId = 0;
        $limit = 200;
        $records = [];
        $loop = 1;
        while (true) {
            $res = self::getObjectRecords($appToken, $namespace, $object, array_merge($postData, [
                'offset' => 0,
                'limit' => $limit,
                'count' => false,
                'filter' => array_merge($postData['filter'] ?? [], [['operator' => 'gt', 'rightValue' => $minId, 'leftValue' => '_id']]),
                'sort' => array_merge($postData['sort'] ?? [], [['field' => '_id', 'direction' => 'asc']]),
            ]));
            $_records = $res->data->records;
            $records = array_merge($records, $_records);
            if (is_callable($callback))
                $callback($loop);
            if (sizeof($_records) < $limit)
                break;
            $minId = $_records[$limit - 1]->_id;
            $loop++;
        }
        return $records;
    }

    /**
     * 获取对象记录详情
     * @param $appToken
     * @param $namespace
     * @param $object
     * @param $recordId
     * @return mixed
     * @throws GuzzleException
     */
    public static function getObjectRecord($appToken, $namespace, $object, $recordId): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_AE, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/api/data/v1/namespaces/$namespace/objects/$object/$recordId", [
            'headers' => [
                'Authorization' => $appToken
            ],
        ]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 编辑对象记录
     * @param $appToken
     * @param $namespace
     * @param $object
     * @param $recordId
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function updateObjectRecord($appToken, $namespace, $object, $recordId, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_AE, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('PATCH', "/api/data/v1/namespaces/$namespace/objects/$object/$recordId", [
            'headers' => [
                'Authorization' => $appToken
            ],
            'json' => $postData
        ]);
        return json_decode($response->getBody()->getContents());
    }
}