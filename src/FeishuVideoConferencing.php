<?php

namespace Spacecat\Feishu;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FeishuVideoConferencing
{
    /**
     * ============= 会议室层级 =============
     */
    /**
     * 视频会议-会议室层级-查询会议室层级列表
     * @param $accessToken
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function vcRoomsQueryRoomLevels($accessToken, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/vc/v1/room_levels", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 视频会议-会议室层级-查询会议室层级列表（全部不分页）
     * @param $accessToken
     * @param array $query
     * @return array
     * @throws GuzzleException
     */
    public static function vcRoomsQueryRoomLevelsAll($accessToken, array $query = []): array
    {
        $rooms = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = self::vcRoomsQueryRoomLevels($accessToken, array_merge([
                'page_token' => $pageToken,
                'page_size' => 100,
            ], $query));
            $hasMore = $res->data->has_more;
            $pageToken = $hasMore ? $res->data->page_token : null;
            $rooms = array_merge($rooms, property_exists($res->data, 'items') ? $res->data->items : []);
        }
        return $rooms;
    }

    /**
     * ============= 会议室管理 =============
     */
    /**
     * 视频会议-会议室管理-查询会议室详情
     * @param $accessToken
     * @param $roomId
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function vcRoomsQueryRoomDetail($accessToken, $roomId, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/vc/v1/rooms/$roomId", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 视频会议-会议室管理-批量查询会议室详情
     * @param $accessToken
     * @param array $query
     * @param array $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function vcRoomsBatchQueryRoomDetail($accessToken, array $postData, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/vc/v1/rooms/mget", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 视频会议-会议室管理-查询会议室列表
     * @param $accessToken
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function vcRoomsQueryRooms($accessToken, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/vc/v1/rooms", ['headers' => [
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 视频会议-会议室管理-查询会议室列表（全部不分页）
     * @param $accessToken
     * @param array $query
     * @return array
     * @throws GuzzleException
     */
    public static function vcRoomsQueryRoomsAll($accessToken, array $query = []): array
    {
        $rooms = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = self::vcRoomsQueryRooms($accessToken, array_merge([
                'page_token' => $pageToken,
                'page_size' => 100,
            ], $query));
            $hasMore = $res->data->has_more;
            $pageToken = $hasMore ? $res->data->page_token : null;
            $rooms = array_merge($rooms, property_exists($res->data, 'rooms') ? $res->data->rooms : []);
        }
        return $rooms;
    }
}