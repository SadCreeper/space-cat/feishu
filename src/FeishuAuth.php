<?php

namespace Spacecat\Feishu;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 认证
 *
 * Class FeishuAuth
 * @package Spacecat\Feishu
 */
class FeishuAuth
{
    /**
     * 获取 app_access_token（企业自建应用）
     *
     * @param $appId
     * @param $appSecret
     * @return mixed
     * @throws GuzzleException
     * @throws \Exception
     */
    public static function getAppAccessTokenInternal($appId, $appSecret): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/auth/v3/app_access_token/internal/', ['json' => [
            'app_id' => $appId,
            'app_secret' => $appSecret,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取 tenant_access_token（企业自建应用）
     *
     * @param $appId
     * @param $appSecret
     * @return mixed
     * @throws GuzzleException
     * @demos:
     * $tenantAccessToken = FeishuAuth::getTenantAccessTokenInternal(env('FS_APP_ID'), env('FS_APP_SECRET'))->tenant_access_token;
     */
    public static function getTenantAccessTokenInternal($appId, $appSecret): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/auth/v3/tenant_access_token/internal/', ['json' => [
            'app_id' => $appId,
            'app_secret' => $appSecret,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 请求用户身份验证
     *
     * @param $redirectUrl
     * @param $appId
     * @param string $state
     * @return string
     */
    public static function getLoginUrl($redirectUrl, $appId, string $state = ''): string
    {
        return sprintf('%s/open-apis/authen/v1/index?redirect_uri=%s&app_id=%s&state=%s', Constant::BASE_URL, $redirectUrl, $appId, $state);
    }

    /**
     * 获取登录用户身份
     *
     * @param $appAccessToken
     * @param $code
     * @return mixed
     * @throws GuzzleException
     * @throws \Exception
     */
    public static function getUserAccessToken($appAccessToken, $code): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/authen/v1/access_token', [
            'headers' => ['Authorization' => 'Bearer ' . $appAccessToken],
            'json' => [
                'grant_type' => 'authorization_code',
                'code' => $code,
            ]
        ]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 刷新 access_token
     *
     * @param $appAccessToken
     * @param $refreshToken
     * @return mixed
     * @throws GuzzleException
     */
    public static function refreshUserAccessToken($appAccessToken, $refreshToken): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/authen/v1/refresh_access_token', [
            'headers' => ['Authorization' => 'Bearer ' . $appAccessToken],
            'json' => [
                'grant_type' => 'refresh_token',
                'refresh_token' => $refreshToken,
            ]
        ]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取用户信息
     *
     * @param $userAccessToken
     * @return mixed
     * @throws GuzzleException
     * @throws \Exception
     */
    public static function getUserData($userAccessToken): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/authen/v1/user_info', [
            'headers' => [
                'Authorization' => 'Bearer ' . $userAccessToken,
            ]
        ]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * --------- 不推荐 ---------------------------------------------
     */

    /**
     * 获取登录用户身份（已弃用，不推荐）
     *
     * @param $appAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function getLoginUserIdentity($appAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/authen/v1/access_token', [
            'headers' => ['Authorization' => 'Bearer ' . $appAccessToken],
            'json' => $postData
        ]);
        return json_decode($response->getBody()->getContents());
    }
}
