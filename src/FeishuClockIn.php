<?php

namespace Spacecat\Feishu;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FeishuClockIn
{
    /**
     * 获取打卡结果
     *
     * @param $tenantAccessToken
     * @param $employeeType
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function getClockInResult($tenantAccessToken, $employeeType, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/attendance/v1/user_tasks/query', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['employee_type' => $employeeType], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 批量查询用户设置
     *
     * @param $tenantAccessToken
     * @param $employeeType
     * @param $userIds
     * @return mixed
     * @throws GuzzleException
     */
    public static function batchQueryUserConfig($tenantAccessToken, $employeeType, $userIds): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/attendance/v1/user_settings/query', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['employee_type' => $employeeType], 'json' => ['user_ids' => $userIds]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 修改用户设置
     *
     * @param $tenantAccessToken
     * @param $employeeType
     * @param $userSetting
     * @return mixed
     * @throws GuzzleException
     */
    public static function updateUserConfig($tenantAccessToken, $employeeType, $userSetting): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/attendance/v1/user_settings/modify', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['employee_type' => $employeeType], 'json' => ['user_setting' => $userSetting]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 上传文件
     *
     * @param $tenantAccessToken
     * @param $fileName
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function uploadFile($tenantAccessToken, $fileName, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/attendance/v1/files/upload', ['headers' => [
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['file_name' => $fileName], 'multipart' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * @param $tenantAccessToken
     * @param $fileId
     * @return string
     * @throws GuzzleException
     */
    public static function downloadFile($tenantAccessToken, $fileId): string
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/attendance/v1/files/$fileId/download", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ]]);
        return $response->getBody()->getContents();
    }
}
