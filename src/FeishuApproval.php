<?php

namespace Spacecat\Feishu;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FeishuApproval
{
    /**
     * 查看审批定义
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function viewApprovalDefinition($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_APPROVAL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', 'v2/approval/get', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 批量获取审批实例
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function batchGetApprovalInstance($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_APPROVAL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', 'v2/instance/list', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 批量获取审批实例（全部不分页）
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return array
     * @throws GuzzleException
     */
    public static function batchGetApprovalInstanceAll($tenantAccessToken, $postData): array
    {
        $instances = [];
        $hasMore = true;
        $offset = 0;
        while ($hasMore) {
            $res = FeishuApproval::batchGetApprovalInstance($tenantAccessToken, array_merge([
                'offset' => $offset,
                'limit' => 100,
            ], $postData));

            if (sizeof($res->{'data'}->{'instance_code_list'}) !== 100) {
                $hasMore = false;
            }
            $offset += 100;
            $instances = array_merge($instances, $res->{'data'}->{'instance_code_list'});
        }
        return $instances;
    }

    /**
     * 获取单个审批实例详情（已弃用，不建议）
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function getSingleApprovalInstanceDetail($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_APPROVAL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', 'v2/instance/get', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取单个审批实例详情
     *
     * @param $tenantAccessToken
     * @param $instanceId
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getSingleApprovalInstanceDetailV4($tenantAccessToken, $instanceId, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/approval/v4/instances/$instanceId", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 创建审批实例
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function createApprovalInstance($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_APPROVAL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', 'v2/instance/create', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 上传文件
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function uploadFile($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_APPROVAL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', 'v2/file/upload', ['headers' => [
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'multipart' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 实例列表查询
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function instanceListSearch($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_APPROVAL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', 'v2/instance/search', ['headers' => [
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }
}
