<?php

namespace Spacecat\Feishu;

use Exception;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class GuzzleRetry
{
    public static function createRetryMiddleware(): callable
    {
        return Middleware::retry(function ($retries, Request $request, Response $response = null, Exception $exception = null) {
            if ($retries >= 5) {
                return false;
            }
            if ($exception instanceof ConnectException) {
                return true;
            }
            if (!$response) {
                return true;
            }
            if (in_array($response->getStatusCode(), [500, 502, 504])) {
                return true;
            }
            if ($response->getStatusCode() === 200) {
                try {
                    $stream = $response->getBody();
                    $responseJson = json_decode($stream->getContents());
                    $stream->rewind();
                    $internalErrorCodes = [
                        1500, 1503, 1642, 1663, 1665, 1668, 2200, 5000,
                        10101, 10105,
                        40003, 40006, 45500,
                        55001,
                        65001,
                        90203, 90399, 93006, 95001, 95003, 95005, 95008,
                        95010, 95011, 95201, 95202, 95203, 95204, 95205,
                        95206, 95207, 95208, 95209, 96001, 96201, 96202,
                        105001,
                        190003,
                        1001901, 1050002, 1069399,
                        99991644,
                    ];
                    if (property_exists($responseJson, 'code') && in_array($responseJson->code, $internalErrorCodes)) {
                        return true;
                    }
                } catch (Exception $e) {

                }
            }
            return false;
        }, function () {
            return 0;
        });
    }

    public static function createHandlerStack(): HandlerStack
    {
        $handlerStack = HandlerStack::create(new CurlHandler());
        $handlerStack->push(self::createRetryMiddleware());
        return $handlerStack;
    }
}
