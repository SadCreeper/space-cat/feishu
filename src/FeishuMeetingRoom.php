<?php

namespace Spacecat\Feishu;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FeishuMeetingRoom
{
    /**
     * 获取会议室日程主题
     *
     * @param $tenantAccessToken
     * @param $eventUids
     * @return mixed
     * @throws GuzzleException
     */
    public static function getMeetingEventTitle($tenantAccessToken, $eventUids): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/meeting_room/summary/batch_get', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => ['EventUids' => $eventUids]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取建筑物列表
     *
     * @param $tenantAccessToken
     * @param $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getBuildingsList($tenantAccessToken, $query): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/meeting_room/building/list', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取建筑物列表（全部不分页）
     *
     * @param $tenantAccessToken
     * @param $query
     * @return array
     * @throws GuzzleException
     */
    public static function getBuildingsListAll($tenantAccessToken, $query): array
    {
        $buildings = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = FeishuMeetingRoom::getBuildingsList($tenantAccessToken, array_merge([
                'page_token' => $pageToken,
                'page_size' => 100,
            ], $query));
            $hasMore = $res->{'data'}->{'has_more'};
            if ($hasMore) {
                $pageToken = $res->{'data'}->{'page_token'};
            }
            $buildings = array_merge($buildings, $res->{'data'}->{'buildings'});
        }
        return $buildings;
    }

    /**
     * 获取会议室列表
     *
     * @param $tenantAccessToken
     * @param $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getMeetingRoomsList($tenantAccessToken, $query): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/meeting_room/room/list', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取会议室列表（全部不分页）
     *
     * @param $tenantAccessToken
     * @param $query
     * @return array
     * @throws GuzzleException
     */
    public static function getMeetingRoomsListAll($tenantAccessToken, $query): array
    {
        $rooms = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = FeishuMeetingRoom::getMeetingRoomsList($tenantAccessToken, array_merge([
                'page_token' => $pageToken,
                'page_size' => 100,
            ], $query));
            $hasMore = $res->{'data'}->{'has_more'};
            if ($hasMore) {
                $pageToken = $res->{'data'}->{'page_token'};
            }
            $rooms = array_merge($rooms, $res->{'data'}->{'rooms'});
        }
        return $rooms;
    }

    /**
     * 查询会议室详情
     *
     * @param $tenantAccessToken
     * @param $roomIds
     * @param null $fields
     * @return mixed
     * @throws GuzzleException
     */
    public static function queryMeetingRoomDetail($tenantAccessToken, $roomIds, $fields = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/meeting_room/room/batch_get', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['room_ids' => $roomIds, 'fields' => $fields]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 查询会议室详情 V2（支持查多个，直接将查询参数拼好传入 $query）
     *
     * @param $tenantAccessToken
     * @param $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function queryMeetingRoomDetailV2($tenantAccessToken, $query): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/meeting_room/room/batch_get', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 会议室忙闲查询
     *
     * @param $tenantAccessToken
     * @param $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function meetingRoomFreeBusyQuery($tenantAccessToken, $query): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/meeting_room/freebusy/batch_get', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }
}
