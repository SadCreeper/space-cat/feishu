<?php

namespace Spacecat\Feishu;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FeishuWebComponent
{
    /**
     * 获取 jsapi_ticket
     * @param $tenantAccessToken
     * @return mixed
     * @throws GuzzleException
     */
    public static function getJsAPITicket($tenantAccessToken): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/jssdk/ticket/get', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ]]);
        return json_decode($response->getBody()->getContents());
    }
}
