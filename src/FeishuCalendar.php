<?php

namespace Spacecat\Feishu;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FeishuCalendar
{
    /**
     * 创建访问控制
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @param $postData
     * @param null $userIdType
     * @return mixed
     * @throws GuzzleException
     */
    public static function createAccessControl($tenantAccessToken, $calendarId, $postData, $userIdType = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/calendar/v4/calendars/$calendarId/acls", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['user_id_type' => $userIdType], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取访问控制列表
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getAccessControlList($tenantAccessToken, $calendarId, $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/calendar/v4/calendars/$calendarId/acls", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 删除访问控制
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @param $aclId
     * @return mixed
     * @throws GuzzleException
     */
    public static function deleteAccessControl($tenantAccessToken, $calendarId, $aclId): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('DELETE', "/open-apis/calendar/v4/calendars/$calendarId/acls/$aclId", ['headers' => [
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 查询主日历信息
     * @param $accessToken
     * @param $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getMainCalendar($accessToken, $query = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/calendar/v4/calendars/primary", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 创建日历
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function createCalendar($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/calendar/v4/calendars', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 删除日历
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @return mixed
     * @throws GuzzleException
     */
    public static function deleteCalendar($tenantAccessToken, $calendarId): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('DELETE', "/open-apis/calendar/v4/calendars/$calendarId", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 查询日历信息
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @return mixed
     * @throws GuzzleException
     */
    public static function getCalendar($tenantAccessToken, $calendarId): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/calendar/v4/calendars/$calendarId", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取日历列表
     *
     * @param $tenantAccessToken
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getCalendarList($tenantAccessToken, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/calendar/v4/calendars", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取日历列表（全部）
     *
     * @param $tenantAccessToken
     * @param array $query
     * @return array
     * @throws GuzzleException
     */
    public static function getCalendarListAll($tenantAccessToken, array $query = []): array
    {
        $calendars = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = FeishuCalendar::getCalendarList($tenantAccessToken, array_merge([
                'page_token' => $pageToken,
                'page_size' => 1000,
            ], $query));
            $hasMore = $res->{'data'}->{'has_more'};
            $pageToken = $hasMore ? $res->{'data'}->{'page_token'} : null;
            $calendars = array_merge($calendars, property_exists($res->data, 'calendar_list') ? $res->data->calendar_list : []);
        }
        return $calendars;
    }

    /**
     * 更新日历
     *
     * @param $accessToken
     * @param $calendarId
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function updateCalendar($accessToken, $calendarId, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('PATCH', "/open-apis/calendar/v4/calendars/$calendarId", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 搜索日历
     *
     * @param $tenantAccessToken
     * @param $keyword
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function searchCalendar($tenantAccessToken, $keyword, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/calendar/v4/calendars/search", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query, 'json' => ['query' => $keyword]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 搜索日历（全部）
     *
     * @param $tenantAccessToken
     * @param $keyword
     * @return array
     * @throws GuzzleException
     */
    public static function searchCalendarAll($tenantAccessToken, $keyword): array
    {
        $calendars = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = FeishuCalendar::searchCalendar($tenantAccessToken, $keyword, [
                'page_token' => $pageToken,
                'page_size' => 50,
            ]);
            $hasMore = $res->{'data'}->{'page_token'};
            $pageToken = $hasMore ? $res->{'data'}->{'page_token'} : null;
            $calendars = array_merge($calendars, property_exists($res->data, 'items') ? $res->data->items : []);
        }
        return $calendars;
    }

    /**
     * 创建日程
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function createEvent($tenantAccessToken, $calendarId, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/calendar/v4/calendars/$calendarId/events", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 删除日程
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @param $eventId
     * @param null $needNotification
     * @return mixed
     * @throws GuzzleException
     */
    public static function deleteEvent($tenantAccessToken, $calendarId, $eventId, $needNotification = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('DELETE', "/open-apis/calendar/v4/calendars/$calendarId/events/$eventId", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['need_notification' => $needNotification]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取日程列表
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getEventList($tenantAccessToken, $calendarId, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/calendar/v4/calendars/$calendarId/events", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取日程列表（全部）
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @param array $query
     * @return array
     * @throws GuzzleException
     */
    public static function getEventListAll($tenantAccessToken, $calendarId, array $query = []): array
    {
        $events = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = FeishuCalendar::getEventList($tenantAccessToken, $calendarId, array_merge([
                'page_token' => $pageToken,
                'page_size' => 1000,
            ], $query));
            $hasMore = $res->{'data'}->{'has_more'};
            $pageToken = $hasMore ? $res->{'data'}->{'page_token'} : null;
            $events = array_merge($events, property_exists($res->data, 'items') ? $res->data->items : []);
        }
        return $events;
    }

    /**
     * 更新日程
     *
     * @param $accessToken
     * @param $calendarId
     * @param $eventId
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function updateEvent($accessToken, $calendarId, $eventId, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('PATCH', "/open-apis/calendar/v4/calendars/$calendarId/events/$eventId", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $accessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * ============= 日程参与人管理（含会议室） =============
     */

    /**
     * 日程参与人管理（含会议室）—— 添加日程参与人
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @param $eventId
     * @param $postData
     * @param null $userIdType
     * @return mixed
     * @throws GuzzleException
     */
    public static function eventParticipantsCreate($tenantAccessToken, $calendarId, $eventId, $postData, $userIdType = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/calendar/v4/calendars/$calendarId/events/$eventId/attendees", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['user_id_type' => $userIdType], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 日程参与人管理（含会议室）—— 添加日程参与人（建议使用新的方法名 eventParticipantsCreate）
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @param $eventId
     * @param $postData
     * @param null $userIdType
     * @return mixed
     * @throws GuzzleException
     */
    public static function createEventParticipant($tenantAccessToken, $calendarId, $eventId, $postData, $userIdType = null): mixed
    {
        return self::eventParticipantsCreate($tenantAccessToken, $calendarId, $eventId, $postData, $userIdType);
    }

    /**
     * 日程参与人管理（含会议室）-删除日程参与人
     *
     * @param $tenantAccessToken
     * @param $calendarId
     * @param $eventId
     * @param $postData
     * @param null $userIdType
     * @return mixed
     * @throws GuzzleException
     */
    public static function eventParticipantsDelete($tenantAccessToken, $calendarId, $eventId, $postData, $userIdType = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/calendar/v4/calendars/$calendarId/events/$eventId/attendees/batch_delete", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['user_id_type' => $userIdType], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 日程参与人管理（含会议室）-获取日程参与人列表
     * @param $tenantAccessToken
     * @param $calendarId
     * @param $eventId
     * @param $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function eventParticipantsGetList($tenantAccessToken, $calendarId, $eventId, $query = null): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/open-apis/calendar/v4/calendars/$calendarId/events/$eventId/attendees", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());

    }

    /**
     * 日程参与人管理（含会议室）-获取日程参与人列表-全部不分页
     * @param $tenantAccessToken
     * @param $calendarId
     * @param $eventId
     * @param array $query
     * @return array
     * @throws GuzzleException
     */
    public static function eventParticipantsGetListAll($tenantAccessToken, $calendarId, $eventId, array $query = []): array
    {
        $participants = [];
        $hasMore = true;
        $pageToken = null;
        while ($hasMore) {
            $res = FeishuCalendar::eventParticipantsGetList($tenantAccessToken, $calendarId, $eventId, array_merge([
                'page_token' => $pageToken,
                'page_size' => 100,
            ], $query));
            $hasMore = $res->{'data'}->{'has_more'};
            $pageToken = $hasMore ? $res->{'data'}->{'page_token'} : null;
            $participants = array_merge($participants, property_exists($res->data, 'items') ? $res->data->items : []);
        }
        return $participants;
    }
}
