<?php


namespace Spacecat\Feishu;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FeishuMessageAndGroup
{
    /**
     * 发送消息
     *
     * @param $tenantAccessToken
     * @param $receiveIdType
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function sendMessage($tenantAccessToken, $receiveIdType, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/im/v1/messages', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['receive_id_type' => $receiveIdType], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 回复消息
     *
     * @param $tenantAccessToken
     * @param $messageId
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function replyMessage($tenantAccessToken, $messageId, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/im/v1/messages/$messageId/reply", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 批量发送消息
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function batchSendMessage($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/message/v4/batch_send/', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 上传图片
     *
     * @param $tenantAccessToken
     * @param $postData
     * @return mixed
     * @throws GuzzleException
     */
    public static function uploadImage($tenantAccessToken, $postData): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/im/v1/images', ['headers' => [
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'multipart' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 创建群聊
     *
     * @param $tenantAccessToken
     * @param $postData
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function createGroup($tenantAccessToken, $postData, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', '/open-apis/im/v1/chats', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 更新群信息
     *
     * @param $tenantAccessToken
     * @param $chatId
     * @param $postData
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function updateGroupData($tenantAccessToken, $chatId, $postData, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('PUT', "/open-apis/im/v1/chats/$chatId", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 获取用户或机器人所在的群列表
     *
     * @param $tenantAccessToken
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getGroupListOfUserOrRobot($tenantAccessToken, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/open-apis/im/v1/chats', ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 将用户或机器人拉入群聊
     *
     * @param $tenantAccessToken
     * @param $chatId
     * @param $postData
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function pullUsersOrRobotsIntoGroup($tenantAccessToken, $chatId, $postData, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('POST', "/open-apis/im/v1/chats/$chatId/members", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => $query, 'json' => $postData]);
        return json_decode($response->getBody()->getContents());
    }
}
