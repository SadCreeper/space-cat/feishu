<?php


namespace Spacecat\Feishu;


class Constant
{
    /**
     * 飞书开放平台地址
     */
    const BASE_URL = 'https://open.feishu.cn';

    /**
     * 飞书开放平台（审批）地址
     */
    const BASE_URL_APPROVAL = 'https://www.feishu.cn/approval/openapi/';

    /**
     * 飞书应用引擎地址
     */
    const BASE_URL_AE = 'https://ae-openapi.feishu.cn';

    /**
     * 飞书成电中间机地址
     */
    const BASE_URL_MIDDLE = 'https://feishumiddle.uestc.edu.cn';

    /**
     * 成电飞书数据中心地址
     */
    const BASE_URL_DATA_CENTER = 'http://222.197.183.12:8080';
}
