<?php


namespace Spacecat\Feishu;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class FeishuMiddle
{
    /**
     * 自定义字段 ID
     */
    const CUSTOM_ATTRS_PAYROLL_NUMBER_ID = 'C-6796546334003298306';// 工资号
    const CUSTOM_ATTRS_STUDENT_NUMBER_ID = 'C-6796546334007492609';// 学号
    const CUSTOM_ATTRS_EXAMINATION_NUMBER_ID = 'C-6864121449213362180';// 考号
    const CUSTOM_ATTRS_MESSAGE_ID = 'C-7122716608979599364';// 备注

    /**
     * 通过学号、工号、考号查询飞书 userid
     *
     * @param $tenantAccessToken
     * @param $number
     * @return mixed
     * @throws GuzzleException
     * @demos:
     * $userId = FeishuMiddle::getUserIdByNumber($tenantAccessToken, '123')->{'data'}->{'employeeId'};
     */
    public static function getUserIdByNumber($tenantAccessToken, $number): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_MIDDLE, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', "/api/FindEmployeeId", ['headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $tenantAccessToken,
        ], 'query' => ['Number' => $number]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 通过学号、工号、考号查询飞书用户（仅含基础信息、需要详细信息拿查到的 user_id 再去飞书查）
     *
     * @param array $numbers
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function getUserByNumbers(array $numbers, array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_DATA_CENTER, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/api/contact/users', [
            'query' => array_merge(['numbers' => $numbers], $query)
        ]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 搜索用户
     *
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function searchUser(array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_DATA_CENTER, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/api/contact/users_search', [
            'query' => $query
        ]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 搜索用户 V2
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public static function searchUserV2(array $query = []): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_DATA_CENTER, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/api/contact/users_search/v2', [
            'query' => $query
        ]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 搜索用户 V3
     * @param string $search
     * @return mixed
     * @throws GuzzleException
     */
    public static function searchUserV3(string $search): mixed
    {
        $client = new Client(['base_uri' => Constant::BASE_URL_DATA_CENTER, 'handler' => GuzzleRetry::createHandlerStack()]);
        $response = $client->request('GET', '/api/contact/users_search/v3', [
            'query' => ['search' => $search]
        ]);
        return json_decode($response->getBody()->getContents());
    }
}
