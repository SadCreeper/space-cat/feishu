<?php


namespace Spacecat\Feishu;


class FeishuUtils
{
    /**
     * 从单个用户信息中提取自定义属性值（废弃，不建议使用）
     *
     * @param $singleUserData
     * @param $CustomAttrFieldId
     * @return mixed|null
     */
    public static function getCustomAttrValueFromSingleUserData($singleUserData, $CustomAttrFieldId): mixed
    {
        $value = null;
        $singleUserData = $singleUserData->{'user'};
        if (property_exists($singleUserData, 'custom_attrs')) {
            foreach ($singleUserData->custom_attrs as $attr) {
                if ($attr->id === $CustomAttrFieldId) {
                    $value = $attr->value->text;
                }
            }
        }
        return $value;
    }

    /**
     * 从单个用户信息中提取自定义属性值
     *
     * @param $singleUser
     * @param $CustomAttrFieldId
     * @return mixed|null
     */
    public static function getCustomAttrValueFromSingleUser($singleUser, $CustomAttrFieldId): mixed
    {
        $value = null;
        if (property_exists($singleUser, 'custom_attrs')) {
            foreach ($singleUser->custom_attrs as $attr) {
                if ($attr->id === $CustomAttrFieldId) {
                    $value = $attr->value->text;
                }
            }
        }
        return $value;
    }

    /**
     * 从审批实例表单中根据字段名获取字段值
     * @param $instanceForm
     * @param array $fieldNames
     * @return mixed|null
     */
    public static function getValueFromInstanceForm($instanceForm, array $fieldNames): mixed
    {
        for ($i = 0; $i < sizeof($instanceForm); $i++) {
            $field = $instanceForm[$i];
            if (in_array($field->name, $fieldNames))
                return $field->value;
        }
        return null;
    }
}
